import requests
import json
import sys
import time
import argparse
from subprocess import Popen, PIPE
from urllib.parse import urlparse

if sys.version_info[0] < 3:
    print('python2 not supported, please use python3')
    sys.exit()

parser = argparse.ArgumentParser(description='Pool payout check script')
parser.add_argument(
    '-t', '--test',
    action='store_true',
    help='Use this flag to run the script in test mode, no transactions will be made',
    required=False
)
args = parser.parse_args()


def send_telegram_message(msg):
    msg = "Payout check could not continue after encountering an error for address " + conf[
        'payout_address'] + ":\n\n" + msg
    try:
        if not telegram_conf["use_telegram"]:
            return

        if len(msg) < 2000:
            uri = urlparse(
                'https://api.telegram.org/bot' + telegram_conf["bot_key"] + '/sendMessage?chat_id=' + telegram_conf[
                    "chat_id"] + '&text=' + msg
            ).geturl()
            requests.get(uri)
        else:
            chunks = int(len(msg) / 2000) + 1
            for i in range(0, chunks):
                uri = urlparse(
                    'https://api.telegram.org/bot' + telegram_conf["bot_key"] + '/sendMessage?chat_id=' + telegram_conf[
                        "chat_id"] + '&text=' + msg[i * chunks: (i + 1) * chunks]
                ).geturl()
                requests.get(uri)
    except Exception as ex:
        handle_error(ex, 'Unable to send telegram message', False)


def handle_error(ex, msg, exit):
    error_msg = time.strftime('%Y-%m-%d %H:%M:%S: ') + msg
    if ex is not None:
        error_msg += ': ' + str(ex)
    print(error_msg)
    if exit:
        send_telegram_message(error_msg)
        sys.exit()


def to_coin(amount):
    return int(amount * 100000000)


def from_coin(amount):
    return amount / 100000000


def get_node_url():
    node_url = conf['node_url']
    if not node_url.startswith('http'):
        handle_error(None, 'node_url needs to be in the format http://localhost:<port>', True)
    if node_url.endswith('/'):
        node_url = node_url[:-1]
    return node_url


def get_balance(node_url, address_delegate):
    try:
        request_url = node_url + '/api/accounts/getBalance?address=' + address_delegate
        response = requests.get(request_url)
        if response.status_code == 200:
            response_json = response.json()
            if response_json['success']:
                return from_coin(int(response_json['balance']))
            else:
                handle_error(None, 'Failed to get balance', True)
        else:
            handle_error(None, str(response.status_code) + ' Failed to get balance', True)
    except Exception as ex:
        handle_error(ex, 'Unable to get balance', True)


def get_to_pay(poollogs):
    to_pay = poollogs['total_payout_this_round']
    payout_done = poollogs['payout_done']
    if not payout_done:
        handle_error(None, "Poollogs payout_done is False, aborting!", True)
    if to_pay == 0.0:
        handle_error(None, "Poollogs total_payout_this_round is zero, aborting!", True)
    return to_pay


def execute_payments(to_pay, payout_address):
    if mode_test:
        print("Executing test payments totaling " + str(to_pay) + " from " + payout_address)
        return
    try:
        p = Popen("bash " + conf['payments_bash_location'], shell=True, stderr=PIPE)
        output, error = p.communicate()
        if p.returncode != 0:
            handle_error(None, 'Executing payments bash failed with error (' + str(p.returncode) + '): ' + error.decode(
                'utf-8'), True)
    except Exception as ex:
        handle_error(ex, 'Executing payments bash failed', True)


def payout_check():
    poollogs = json.load(open(conf['poollogs_location'], 'r'))
    to_pay = get_to_pay(poollogs)
    node_url = get_node_url()
    payout_address = conf['payout_address']
    balance = get_balance(node_url, payout_address)
    if to_pay > balance:
        handle_error(None, "Not enough balance (" + str(to_pay) + "/" + str(
            balance) + ") on payout address " + payout_address + ", aborting!", True)
    execute_payments(to_pay, payout_address)


mode_test = args.test
try:
    conf = json.load(open('config.json', 'r'))
    telegram_conf = conf['telegram_settings']
except Exception as ex:
    handle_error(ex, 'Unable to load config file', True)
try:
    payout_check()
except Exception as ex:
    handle_error(ex, 'Payout check failed with generic exception', True)
